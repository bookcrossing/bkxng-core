<div class="error-wrap-arrow">
<h2 class="error-page-title">404 страница не найдена</h2>
<div class="error-no-page">
</div>
<div class="error-page-description">
<p>Такой страницы здесь нет... и, возможно, никогда не было.</p>
<p>Пролистайте <a href="books">каталог</a>, или воспользуйтесь <span class="uline">поиском</span></p>
</div>
</div>